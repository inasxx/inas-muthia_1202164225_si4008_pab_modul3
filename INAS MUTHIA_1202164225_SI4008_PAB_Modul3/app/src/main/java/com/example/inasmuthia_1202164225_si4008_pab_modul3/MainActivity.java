package com.example.inasmuthia_1202164225_si4008_pab_modul3;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.text.Layout;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toolbar;

public class MainActivity extends AppCompatActivity {
    Toolbar toolbar;
    Button btnAdd;
    AlertDialog.Builder dialog;
    LayoutInflater inflater;
    View dialogView;
    EditText edtNama, edtPekerjaan;
    Spinner spnJK;
    String nama, pekerjaan, jk;
    RecyclerView rv;
    CardView cv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnAdd = (Button) findViewById(R.id.btnAdd);
        btnAdd.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                cv.setCardBackgroundColor(null);
                DialogForm();
            }
        });
    }


    private void DialogForm() {
        dialog = new AlertDialog.Builder(MainActivity.this);
        inflater = getLayoutInflater();
        dialogView = inflater.inflate(R.layout.activity_form_data, null);
        dialog.setView(dialogView);
        dialog.setCancelable(true);

        edtNama = (EditText) dialogView.findViewById(R.id.edtNama);
        edtPekerjaan = (EditText) dialogView.findViewById(R.id.edtPekerjaan);
        spnJK = (Spinner) dialogView.findViewById(R.id.spnJK);

        dialog.setPositiveButton("SUBMIT", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                nama = edtNama.getText().toString();
                pekerjaan = edtPekerjaan.getText().toString();
                jk = spnJK.getContext().toString();

                //rv.setRecycledViewPool(cv);
                dialog.dismiss();
            }
        });

        dialog.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }


    public void dialogAlert(View view) {
        Intent intent = new Intent(MainActivity.this, FormData.class);
    }

}
